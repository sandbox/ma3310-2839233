<?php

/**
 * @file
 * Template file for UITweak Widget: Site Footer.
 * @Created: 30/12/2016 11:48 PM
 */
?>

<div id="uitweak_widget_site_footer_wrap" style="background-color: <?php print($contents['bgcolor']); ?>">
  <div class="uitweak_widget_site_footer_container">
    <div class="uitweak_widget_site_footer_main">
      <h4 class="uitweak_widget_site_footer_title"><?php print($contents['title']); ?></h4>
      <p><?php print($contents['introduction']); ?></p>
    </div>
    <div class="uitweak_widget_site_footer_menu_wrap">
      <h4 class="uitweak_widget_site_footer_title_place_hold">&nbsp;</h4>
      <div class="uitweak_widget_site_footer_menu uitweak_widget_site_footer_menu_2st">
        <?php if (is_array($contents['menu_2'])): ?>
          <?php foreach ($contents['menu_2'] as $link): ?>
            <a href="<?php print $link['link']; ?>" title="<?php print $link['title']; ?>" <?php print (($link['target']) ? 'target="_blank"' : ''); ?> ><?php print $link['name']; ?></a>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
      <div class="uitweak_widget_site_footer_menu uitweak_widget_site_footer_menu_1st">
        <?php if (is_array($contents['menu_1'])): ?>
          <?php foreach ($contents['menu_1'] as $link): ?>
            <a href="<?php print $link['link']; ?>" title="<?php print $link['title']; ?>" <?php print (($link['target']) ? 'target="_blank"' : ''); ?> ><?php print $link['name']; ?></a>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="uitweak_copyright"><?php print($contents['copyright']); ?></div>
  </div>
</div>
