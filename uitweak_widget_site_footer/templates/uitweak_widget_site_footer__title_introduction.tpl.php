<?php

/**
 * @file
 * Template file for UITweak Widget: Site Footer.
 *
 * @Created: 30/12/2016 11:48 PM
 */
?>
<div id="uitweak_widget_site_footer_wrap" style="background-color: <?php print($contents['bgcolor']); ?>">
  <div class="uitweak_widget_site_footer_container">
    <div class="uitweak_widget_site_footer_main">
      <h4 class="uitweak_widget_site_footer_title"><?php print($contents['title']); ?></h4>
      <p><?php print($contents['introduction']); ?></p>
      <div class="uitweak_copyright"><?php print($contents['copyright']); ?></div>
    </div>
  </div>
</div>
