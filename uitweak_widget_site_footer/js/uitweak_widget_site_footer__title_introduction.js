/**
 * Created on 30/12/2016.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.uitweakWidgetSiteFooter = {
    attach: function (context, settings) {

      $('#uitweak_widget_site_footer_wrap', context).once('uitweakWidgetSiteFooter', function () {

        var uitweak_footer_position_fixed = false;

        var body_height = $(context.body).height();
        var fixed_height = uitweak_footer_position_fixed ? $('#uitweak_widget_site_footer_wrap', context).height() : 0;

        if ((body_height + fixed_height) < $(window).height()) {
          $('footer', context).css('position', 'absolute').css('bottom', '0');
          uitweak_footer_position_fixed = true;
        }

        $(window).resize(function () {
          // change footer position when content is not enough
          var body_height = $(context.body).height();
          var fixed_height = uitweak_footer_position_fixed ? $('#uitweak_widget_site_footer_wrap', context).height() : 0;

          if ((body_height + fixed_height) < $(window).height()) {
            $('footer', context).css('position', 'absolute').css('bottom', '0');
            uitweak_footer_position_fixed = true;
          }
          else {
            $('footer', context).css('position', 'inherit').css('bottom', 'inherit');
            uitweak_footer_position_fixed = false;
          }

        });

      });
    }
  };
})(jQuery);
