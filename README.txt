README.txt for UITweak

Overview

    UITweak provides a set of APIs and utilities to help site builders and
    developers to tweak and build Drupal UI.

    For the moment, UITweak includes the following things:

        - UITweak Commons:
          * provides a set of APIs which are needed by other sub-modules, which
            can simplificate develop works. It mainly includes below functions:

            > uitweak_form_image_process: helps developers to handle image
              through Form API(managed_file element).

        - UITweak Widget - Site Banner:
          * provides several different configurable blocks which can be used as
            site banner. Recommend use blocks with a customized theme basing
            Bootstrap theme(https://www.drupal.org/project/bootstrap), 
            otherwise very probably CSS need to be redefined per breakpoints
            of the theme which is being used.

        - UITweak Widget - Site Footer:
          * provides several different configurable blocks which can be used as
            site footer. Recommend use blocks with a customized theme basing
            Bootstrap theme(https://www.drupal.org/project/bootstrap),
            otherwise very probably CSS need to be redefined per breakpoints
            of the theme which is being used.

Images used in this module are downloaded from below, please support authors:
    https://pixabay.com/en/spiral-logo-whirlpool-1682201/
    https://pixabay.com/en/dab-color-spot-logo-art-splash-1692452/
    https://pixabay.com/en/red-circle-logo-round-element-1618916/
    https://pixabay.com/en/creative-commons-cc-characters-785334/
