<?php

/**
 * @file
 * Template for UITweak Widget: Site Banners.
 * @Variables $contents
 */
?>
<div class="uitweak_widget_site_banner_wrap"
     style="background-color: <?php print($contents['background_color']); ?>;">
  <div class="uitweak_widget_site_banner"
       style="height: <?php print($contents['height']); ?>;">
    <a href="/" title="<?php print($contents['logo_title']); ?>">
      <img class="uitweak_widget_site_banner__logo"
         src="<?php print($contents['logo_url']); ?>"
         alt="<?php print($contents['logo_alt']); ?>"
         title="<?php print($contents['logo_title']); ?>"
      />
    </a>
  </div>
</div>
